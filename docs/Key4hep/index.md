# Using Key4hep

Key4hep is a stack of software packages useful in HEP covering a wide range of applications (event generation, reconstruction, vizualisation, ...). The complete Key4hep documentation can be found [here](https://key4hep.github.io/key4hep-doc/), this page is condensing some information for a impatient users.
The Key4hep team is taking care of building these packages in a consistent way and deliver on a regular basis the so-called "stable stack" releases. To access all these packages from your terminal session, it is enough to call:

`source /cvmfs/sw.hsf.org/key4hep/setup.sh`

NB: to know what packages are available in the Key4hep environment, which is based on the [Spack](https://spack.io/) package manager but has its own fork for agility, take a look at the [Key4hep-spack](https://github.com/key4hep/key4hep-spack/tree/main/packages) and [Spack](https://github.com/spack/spack/tree/develop/var/spack/repos/builtin/packages) repositories. 

Sometimes, we rely on a feature which was developped after the stable stack was released. For this, there is a mechanism that re-builds a complete stack every night by using the `master/main` branch of each git repository included in Key4hep. The so-called 'nightly build' can be accessed with:

`source /cvmfs/sw-nightlies.hsf.org/key4hep/setup.sh`

Mind that this environment does not guarantee stability, some things might be broken due to ever evolving components relying on each other. To list all the 'nigthlies' available, for instane to re-use one that was working for your purpose, use:

`ls -lah /cvmfs/sw-nightlies.hsf.org/key4hep/releases`

# Developing in Key4hep

Soon you will have the need to develop new features. To do so there are a few useful things to know:


1. Once a given environment has been sourced, the various packages will be taken from the 'cenrtral' libraries available in this environment. If you want to apply modifications to a given package e.g. a detector in [k4geo](https://github.com/key4hep/k4geo) or a reconstruction algorithm in [k4RecCalorimeter](https://github.com/HEP-FCC/k4RecCalorimeter), clone it locally (in a public folder so that it is easier for the support team to help you in debugging), apply your changes and build it with e.g.

    ```
    mkdir build install
    cd build
    cmake .. -DCMAKE_INSTALL_PREFIX=../install
    make install -j 8
    cd ..
    k4_local_repo
    ```

    The `k4_local_repo` command makes sure the relevant environment variables are properly updated in order to pick-up your modified local version of the package instead of the central one.


2. When a given Gaudi Configurables fails, it is not always easy to know which repository hosts it. This command will help you finding this infromation:

    `k4run -l` lists all the configurables and their orignal location. The list being quite long, you may want to pipe it to grep. For instance, if you want to know where `GeoSvc` is living call:

    `k4run -l | grep -A 1 GeoSvc`

3. Developing in a large framework which is used by many people requires some discipline to keep consistency accross the various components. Try to write code as clean as you can, document it as much as you can with README's and with inline comments (your present and future collaborators will thank you for that), add tests for newly introduced features and run `clang-format` for syntax.
